package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class OfficeTest {

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void should_create_office_entity() {
        assertTrue(true);
    }

    @Test
    void should_save_an_entity() {
        Office office = officeRepository.save(new Office(1L, "chengdu"));
        assertNotNull(office);
    }

    @Test
    void should_save_an_entity_using_entity_manager() {
        entityManager.persist(new Office(1L, "Xian"));
        entityManager.flush();
        entityManager.clear();
        Office office = entityManager.find(Office.class, 1L);

        assertEquals(1L, office.getId());
        assertEquals("Xian", office.getCity());
    }
}
